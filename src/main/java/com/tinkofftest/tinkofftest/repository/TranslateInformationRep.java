package com.tinkofftest.tinkofftest.repository;

import com.tinkofftest.tinkofftest.models.TranslateInformation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TranslateInformationRep extends CrudRepository<TranslateInformation, Long> {
}
