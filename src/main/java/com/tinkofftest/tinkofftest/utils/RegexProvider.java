package com.tinkofftest.tinkofftest.utils;

import java.util.ResourceBundle;

public class RegexProvider {
    private static final ResourceBundle TEXT_EXPRESSION = ResourceBundle.getBundle("expressions/text_expressions");

    public static String getTextExpression(String key) {
        return TEXT_EXPRESSION.getString(key);
    }

}
