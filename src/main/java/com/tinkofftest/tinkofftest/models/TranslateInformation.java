package com.tinkofftest.tinkofftest.models;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Entity
@Table(name = "TranslateInformation")
public class TranslateInformation {
    private static final String DATE_FORMAT = "dd.MM.yyyy'T'HH:mm";

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "text", nullable = false)
    private String text;
    @Column(name = "from_language", nullable = false, length = 10)
    private String from;
    @Column(name = "to_language", nullable = false, length = 10)
    private String to;
    @Column(name = "ip_address", nullable = false, length = 20)
    private String ipAddress;
    @Column (name = "request_date", nullable = false)
    private LocalDateTime timeCall;

    public TranslateInformation() {
    }

    public TranslateInformation(String text, String from, String to, String ipAddress) {
        this.text = text;
        this.from = from;
        this.to = to;
        this.ipAddress = ipAddress;
        timeCall = LocalDateTime.now();
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLocalDateTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.ENGLISH);

        return timeCall.format(dtf);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

}
