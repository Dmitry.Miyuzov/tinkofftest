package com.tinkofftest.tinkofftest.controllers;

import com.google.gson.JsonObject;
import com.tinkofftest.tinkofftest.models.TranslateInformation;
import com.tinkofftest.tinkofftest.repository.TranslateInformationRep;
import com.tinkofftest.tinkofftest.utils.GoogleTranslate;
import com.tinkofftest.tinkofftest.utils.RegexProvider;
import com.tinkofftest.tinkofftest.utils.RequestHelper;
import com.tinkofftest.tinkofftest.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
public class TranslateController {
    private static final String PATTERN_TEXT_EXPRESSIONS = RegexProvider.getTextExpression("text.regexp");
    private static final String TEMPLATE_TRANSLATE = "translate";
    private static final Pattern TEXT_PATTERN = Pattern.compile(PATTERN_TEXT_EXPRESSIONS);
    private static final String PARAMETER_TEXT = "text";
    private static final String PARAMETER_FROM = "from";
    private static final String PARAMETER_TO = "to";

    @Autowired
    private TranslateInformationRep translateInformationRep;

    @GetMapping("/translate")
    public String translate(Model model) {
        return TEMPLATE_TRANSLATE;
    }

    @PostMapping("/translate")
    public String executeTranslate(HttpServletRequest request, HttpServletResponse response, Model model) {
        TranslateInformation translateInformation = getDataFromRequest(request);

        if (isValidText(translateInformation.getText())) {
            translateInformationRep.save(translateInformation);
            String textTranslated = getTranslate(translateInformation);
            ResponseHelper.sendResponse(response, createResponse(textTranslated));
            response.setStatus(HttpServletResponse.SC_OK);
        }

        return TEMPLATE_TRANSLATE;
    }

    private boolean isValidText(String text) {
        boolean isValidate = false;
        Matcher matcher = TEXT_PATTERN.matcher(text);
        if (matcher.find()) {
            isValidate = true;
        }
        return isValidate;
    }

    private TranslateInformation getDataFromRequest(HttpServletRequest request) {
        JsonObject jsonObject = RequestHelper.getRequestData(request);
        String from = jsonObject.get(PARAMETER_FROM).getAsString();
        String to = jsonObject.get(PARAMETER_TO).getAsString();
        String text = jsonObject.get(PARAMETER_TEXT).getAsString();
        String ipAddress = request.getRemoteAddr();
        return new TranslateInformation(text, from, to, ipAddress);
    }

    private String getTranslate(TranslateInformation translateInformation) {
        GoogleTranslate googleTranslate = new GoogleTranslate(translateInformation);
        return googleTranslate.executeTranslate();
    }

    private JsonObject createResponse(String text) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(PARAMETER_TEXT, text);
        return jsonObject;
    }


}
