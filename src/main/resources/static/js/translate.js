let inputText = "#input_text_translation";
let outputText = "#output_text_translation";
let fromLanguage = "#choose-language-input";
let toLanguage = "#choose-language-output";
let submitTranslate = "#submit-translate";
const patternText = "^[A-Za-zА-Яа-яЁё0-9\\s\\W][A-Za-zа-яА-ЯЁё0-9\\s\\W]{0,249}$";

let translate = function () {
    let translateSetting = JSON.stringify({
        'text': $(inputText).val().trim(),
        'from': $(fromLanguage).val().trim(),
        'to': $(toLanguage).val().trim()
    });
    let pattern = new RegExp(patternText);
    if (pattern.test(translateSetting)) {
        $.ajax({
            url: "/translate",
            cache: false,
            method: "post",
            contentType: "application/json ; charset=utf-8",
            data: translateSetting,
            dataType: "json",

            statusCode: {
                200: function (jsonFromServer) {
                    $(outputText).val(jsonFromServer.text);
                }
            }
        });
    }
};

let changeColorHoverButton = function (target, commonColor, targetColor) {
    $(document).on({
        mouseenter: function () {
            $(target).css("background-color", targetColor);
        },
        mouseleave: function () {
            $(target).css("background-color", commonColor);
        }
    }, target);
};
let changeColorClickButton = function(target, commonColor, targetColor) {
    $(document).on({
        mousedown: function () {
            $(target).css("background-color", targetColor);
        },
        mouseup: function () {
            $(target).css("background-color", commonColor);
        }
    }, target);
};

let clearField = function (targetClick, clearField) {
    $(document).on("click", targetClick, function () {
        $(clearField).val('');
    });
};

$(document).ready(function () {
    $(document.body).on("click", submitTranslate, function () {
        translate();
    });

    changeColorHoverButton(submitTranslate, 'white', 'gray');
    changeColorClickButton(submitTranslate, 'white', 'green');
    clearField(submitTranslate, outputText);
});